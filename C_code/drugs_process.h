/*  drugs_process.h

This is the header for drugs_process.cpp module. Include this h-file into your code and call 
process_datasets with file path arguments:
char* set_A - small size set with each line representing a few kB query (i.e. dataset A (profiles?/Analysis Tasks))
char* set_B - large (up to 5 Mb) set   (i.e. dataset B (Base Datasets?))
char* out_file - file name with file path to output results to. 

Any questions - please contact Dr. Ivan Laponogov, i.laponogov@imperial.ac.uk

Project DRUGS for Vodafone. 2017

Author: Dr. Ivan Laponogov
Supervisor: Dr. Kirill A. Veselkov

Copyright 2018 Imperial College London

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


*/



int process_datasets(char* set_A, char* set_B, char* out_file);